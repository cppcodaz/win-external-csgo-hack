#pragma once

#include "Includes.h"
#include "MemoryManager.h"


struct EnumData {
	DWORD process_id;
	HWND hwnd;
};

BOOL CALLBACK EnumProcess(HWND hwnd, LPARAM lparam) {
	
	EnumData &ed = *(EnumData*)lparam;
	DWORD process_id = 0x0;

	GetWindowThreadProcessId(hwnd, &process_id);

	if (ed.process_id = process_id) {
		ed.hwnd = hwnd;
		SetLastError(ERROR_SUCCESS);
		return FALSE;
	}

	return TRUE;
}

HWND FindWindowFromProcessId(DWORD process_id) {
	
	EnumData ed = {
		process_id
	};

	if (!EnumWindows(EnumProcess, (LPARAM)&ed) && (GetLastError() == ERROR_SUCCESS)) {
		return ed.hwnd;
	}

	return NULL;
}

HWND FindWindowFromProcess(HANDLE process_handle) {

	return FindWindowFromProcessId(GetProcessId(process_handle));
}

bool InGameWindow(MemoryManager* memory_manager) {

	DWORD game_pid = memory_manager->getProcessId();
	DWORD current_pid;

	GetWindowThreadProcessId(GetForegroundWindow(), &current_pid);

	return game_pid == current_pid;
}