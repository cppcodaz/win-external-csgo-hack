#pragma once

#include "Includes.h"
#include "MemoryManager.h"
#include "Config.h"
#include "Wallhack.h"
#include "Radar.h"
#include "LocalPlayer.h"
#include "UpdateCLIDisplay.h"

// Wallhack and Radar Hack
void VisionThread(MemoryManager* memory_manager, LocalPlayer local_player) {

	bool Changed_Config = false;

	if (GetAsyncKeyState(Config.Wallhack_Key) || GetAsyncKeyState(Config.Radar_Key)) {
		if (InGameWindow(memory_manager)) {
			if (GetAsyncKeyState(Config.Wallhack_Key)) Config.Wallhack_Enabled = !Config.Wallhack_Enabled;

			if (GetAsyncKeyState(Config.Radar_Key)) Config.Radar_Enabled = !Config.Radar_Enabled;

			Changed_Config = true;
		}
	}

	if (Changed_Config) {
		UpdateCLIDisplay();
		Sleep(250);
		Changed_Config = false;
	}

	if (Config.Wallhack_Enabled) Wallhack(memory_manager, local_player);
	if (Config.Radar_Enabled) Radar(memory_manager, local_player);
}