#pragma once

#include "Includes.h"

struct Config {

	bool Wallhack_Enabled = false;
	int Wallhack_Key = VK_F1; // F1

	bool Triggerbot_Enabled = false;
	int Triggerbot_Key = VK_MBUTTON; // Mousewheel

	bool Radar_Enabled = false;
	int Radar_Key = VK_F2; // F2
} Config;