#pragma once

#include "Includes.h"

struct Offset {
	std::string name;
	int value;

	Offset(std::string name, int value) {
		this->name = name;
		this->value = value;
	}
};

struct Offsets {

	std::vector<Offset> offsets;

	int get(const std::string& name) const {
		for (const auto& offset : this->offsets) {
			if (offset.name == name) {
				return offset.value;
			}
		}

		return NULL;
	}

	int set(std::string name, int value) {
		this->offsets.push_back(Offset(name, value));
	}

} Offsets;