#include "stdafx.h"
#include "MemoryManager.h"


MemoryManager::MemoryManager() : process_handle(NULL), process_id(NULL) {
}

bool MemoryManager::attach_process(char* process_name) {

	HANDLE process_id_handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	PROCESSENTRY32 process_entry;
	process_entry.dwSize = sizeof(process_entry);

	const WCHAR* process_name_char;
	int chars = MultiByteToWideChar(CP_ACP, 0, process_name, -1, NULL, 0);
	process_name_char = new WCHAR[chars];
	MultiByteToWideChar(CP_ACP, 0, process_name, -1, (LPWSTR)process_name_char, chars);

	do {
		
		if (!wcscmp(process_entry.szExeFile, process_name_char)) {

			this->process_id = process_entry.th32ProcessID;
			CloseHandle(process_id_handle);

			this->process_handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, this->process_id);

			delete process_name_char;
			return true;
		}
	} while (Process32Next(process_id_handle, &process_entry));

	CloseHandle(process_id_handle);
	delete process_name_char;

	return false;
}

MODULEENTRY32 MemoryManager::get_module(char* module_name) {

	HANDLE module_handle = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, this->process_id);

	MODULEENTRY32 module_entry;
	module_entry.dwSize = sizeof(module_entry);

	const WCHAR *module_name_char;
	int chars = MultiByteToWideChar(CP_ACP, 0, module_name, -1, NULL, 0);
	module_name_char = new WCHAR[chars];
	MultiByteToWideChar(CP_ACP, 0, module_name, -1, (LPWSTR)module_name_char, chars);

	do {

		if (!wcscmp(module_entry.szModule, module_name_char)) {

			CloseHandle(module_handle);
			delete module_name_char;

			return module_entry;
		}
	} while (Module32Next(module_handle, &module_entry));

	CloseHandle(module_handle);
	module_entry.modBaseAddr = 0x0;

	delete module_name_char;

	return module_entry;
}

bool MemoryManager::fetch_process(char* process_name) {

	if (this->attach_process(process_name)) {

		this->client_dll = this->get_module("client.dll");
		this->client_dll_base = (DWORD) this->client_dll.modBaseAddr;

		this->engine_dll = this->get_module("engine.dll");
		this->engine_dll_base = (DWORD) this->engine_dll.modBaseAddr;

		if (this->client_dll_base != 0x0 && this->engine_dll_base != 0x0) {
			this->client_dll_size = this->client_dll.dwSize;
			this->engine_dll_size = this->engine_dll.dwSize;

			return true;
		}
	}

	return false;
}

MemoryManager::~MemoryManager() {
	CloseHandle(this->process_handle);
}