#pragma once

#include "Includes.h"
#include "MemoryManager.h"
#include "Offsets.h"
#include "LocalPlayer.h"

void Radar(MemoryManager* memory_manager, LocalPlayer local_player) {
	
	for (int i = 0; i <= 32; i++) {

		int player_base = memory_manager->read<int>((memory_manager->getClientDLLBase() + Offsets.get("m_dwEntityList")) + (i * Offsets.get("m_EntitySize")));
		int player_team = memory_manager->read<int>(player_base + Offsets.get("m_iTeamNum"));

		if (player_team != local_player.getTeam()) {
			memory_manager->write<int>(player_base + Offsets.get("m_bSpotted"), 1);
		}
	}

}