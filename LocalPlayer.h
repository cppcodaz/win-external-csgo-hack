#pragma once

#include "Includes.h"
#include "MemoryManager.h"
#include "Offsets.h"

class LocalPlayer {

private:
	MemoryManager* memory_manager;

	int base = 0;
	int state = 0;
	int team = 0;
	int index_number = 0;

public:
	LocalPlayer(MemoryManager* memory_manager) {
		this->memory_manager = memory_manager;
	}

	inline int getBase() {

		return this->memory_manager->read<int>(this->memory_manager->getClientDLLBase() + Offsets.get("m_dwLocalPlayer"));
	}

	inline int getState() {

		return this->memory_manager->read<int>(this->memory_manager->getEngineDLLBase() + Offsets.get("dwClientState"));
	}

	inline int getTeam() {

		return this->memory_manager->read<int>(this->getBase() + Offsets.get("m_iTeamNum"));
	}

	inline int getIndexNumber() {

		return this->memory_manager->read<int>(this->getState() + Offsets.get("m_dwLocalPlayerIndex"));
	}

	inline int getInCross() {

		return this->memory_manager->read<int>(this->getBase() + Offsets.get("m_iCrossHairID"));
	}
};