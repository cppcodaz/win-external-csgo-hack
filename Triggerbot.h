#pragma once

#include "MemoryManager.h"
#include "LocalPlayer.h"
#include "Offsets.h"

void Triggerbot(MemoryManager* memory_manager, LocalPlayer local_player) {

	int local_player_inCross = local_player.getInCross();

	int player_base = memory_manager->read<int>(memory_manager->getClientDLLBase() + Offsets.get("m_dwEntityList") + ((local_player_inCross - 1) * Offsets.get("m_EntitySize")));
	int player_team = memory_manager->read<int>(player_base + Offsets.get("m_iTeamNum"));
	bool player_dormant = memory_manager->read<bool>(player_base + Offsets.get("m_bDormant"));

	if ((local_player_inCross > 0 && local_player_inCross <= 64) && player_base != NULL && player_team != local_player.getTeam() && !player_dormant) {
		Sleep(10); 
		mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
		Sleep(10); 
		mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
		Sleep(10); 
	}
}