#pragma once

#include "Includes.h"
#include "MemoryManager.h"
#include "LocalPlayer.h"
#include "PlayerEntity.h"
#include "Offsets.h"

void Wallhack(MemoryManager* memory_manager, LocalPlayer local_player) {
	
	std::vector<PlayerEntity> player_entities;
	
	for (int i = 0; i <= 32; i++) {

		//if (i == local_player.getIndexNumber()) continue;

		PlayerEntity p;
		p.base = memory_manager->read<int>((memory_manager->getClientDLLBase() + Offsets.get("m_dwEntityList")) + (i * Offsets.get("m_EntitySize")));
		p.glowIndex = memory_manager->read<int>(p.base + Offsets.get("m_iGlowIndex"));
		p.team = memory_manager->read<int>(p.base + Offsets.get("m_iTeamNum"));
		p.health = memory_manager->read<int>(p.base + Offsets.get("m_iHealth"));

		player_entities.push_back(p);
	}

	for (auto &p : player_entities) {

		if (p.base == 0) continue;

		int r = !(p.team == local_player.getTeam()) ? (255 * (100 - p.health)) / 100 : 30;
		int g = !(p.team == local_player.getTeam()) ? (255 * p.health) / 100 : 144;
		int b = !(p.team == local_player.getTeam()) ? 0 : 255;
		int a = 210;

		int glow_pointer = memory_manager->read<int>(memory_manager->getClientDLLBase() + Offsets.get("m_dwGlowObjectManager"));

		memory_manager->write<float>(glow_pointer + ((p.glowIndex * 0x38) + 0x4), r / 255.0F);
		memory_manager->write<float>(glow_pointer + ((p.glowIndex * 0x38) + 0x8), g / 255.0F);
		memory_manager->write<float>(glow_pointer + ((p.glowIndex * 0x38) + 0xC), b / 255.0F);
		memory_manager->write<float>(glow_pointer + ((p.glowIndex * 0x38) + 0x10), a / 255.0F);

		memory_manager->write<bool>(glow_pointer + ((p.glowIndex * 0x38) + 0x24), true);
		memory_manager->write<bool>(glow_pointer + ((p.glowIndex * 0x38) + 0x25), false);
		memory_manager->write<bool>(glow_pointer + ((p.glowIndex * 0x38) + 0x26), false);
	}
}