#pragma once

#include "Includes.h"
#include "Config.h"

struct Colors {
	int active = 47;
	int inactive = 79;
	int default = 15;
} Colors;

HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

void Write(const char* text, bool status) {

	std::cout << "\n" << text;
	SetConsoleTextAttribute(hConsole, status ? Colors.active : Colors.inactive);
	std::cout << (!status ? "in" : "") << "aktiv";
	SetConsoleTextAttribute(hConsole, Colors.default);

}

void UpdateCLIDisplay() {

	system("cls");

	// Wallhack
	Write("Wallhack\t\tF1\t", Config.Wallhack_Enabled);

	// Triggerbot
	Write("Triggerbot\t\tMausrad\t", Config.Triggerbot_Enabled);

	// Radar Hack
	Write("Radar Hack\t\tF2\t", Config.Radar_Enabled);
}