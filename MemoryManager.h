#pragma once

#include "Includes.h"

class MemoryManager
{
private:
	DWORD process_id;
	HANDLE process_handle;

	MODULEENTRY32 client_dll;
	DWORD client_dll_base;
	DWORD client_dll_size;

	MODULEENTRY32 engine_dll;
	DWORD engine_dll_base;
	DWORD engine_dll_size;

	bool attach_process(char*);

	MODULEENTRY32 get_module(char*);

public:
	MemoryManager();

	DWORD getProcessId() { return this->process_id; }
	HANDLE getProcessHandle() { return this->process_handle; }

	MODULEENTRY32 getClientDLL() { return this->client_dll; }
	DWORD getClientDLLBase() { return this->client_dll_base; }
	DWORD getClientDLLSize() { return this->client_dll_size; }


	MODULEENTRY32 getEngineDLL() { return this->engine_dll; }
	DWORD getEngineDLLBase() { return this->engine_dll_base; }
	DWORD getEngineDLLSize() { return this->engine_dll_size; }

	template<class c>
	c read(DWORD address) {

		c value;
		ReadProcessMemory(this->process_handle, (LPVOID)address, &value, sizeof(c), NULL);
		return value;
	}

	template<class c>
	BOOL write(DWORD address, c value) {

		return WriteProcessMemory(this->process_handle, (LPVOID)address, &value, sizeof(c), NULL);
	}

	bool fetch_process(char*);

	~MemoryManager();
};