#pragma once

struct PlayerEntity {
	int health;
	int team;
	int glowIndex;
	int base;
	int rank;
	int wins;
};
