#pragma once

#include "Includes.h"
#include "MemoryManager.h"
#include "LocalPlayer.h"
#include "Config.h"
#include "InGameWindow.h"
#include "Triggerbot.h"

// Triggerbot
void ActivityThread(MemoryManager* memory_manager, LocalPlayer local_player) {

	bool Changed_Config = false;

	if ((GetKeyState(Config.Triggerbot_Key) & 0x100) != 0) {
		if (InGameWindow(memory_manager)) {
			if (!Config.Triggerbot_Enabled) {
				Config.Triggerbot_Enabled = true;
				Changed_Config = true;
			}
		}
	}
	else {
		if (Config.Triggerbot_Enabled) {
			Config.Triggerbot_Enabled = false;
			Changed_Config = true;
		}
	}

	if (Changed_Config) {
		UpdateCLIDisplay();
		Changed_Config = false;
	}

	if (Config.Triggerbot_Enabled) Triggerbot(memory_manager, local_player);
}