#include "stdafx.h"

#include "Includes.h"
#include "MemoryManager.h"
#include "InGameWindow.h"
#include "Config.h"
#include "VisionThread.h"
#include "ActivityThread.h"
#include "LocalPlayer.h"
#include "UpdateCLIDisplay.h"
#include "INIReader.h"

#include <fstream>
#include <string>

int main()
{
	SetConsoleTitle(TEXT("private external Hack - lade offsets"));
	std::cout << "Offsets werden geladen ..." << std::endl;

	INIReader reader("Offsets.ini");

	std::vector<std::string> offsets {
		"m_dwLocalPlayer", 
		"m_iTeamNum", 
		"m_dwLocalPlayerIndex", 
		"m_dwEntityList", 
		"m_EntitySize", 
		"m_iGlowIndex", 
		"m_iHealth", 
		"m_dwGlowObjectManager", 
		"m_bSpotted",
		"m_iCrossHairID",
		"m_bDormant",
		"dwClientStat"
	};
	
	for (const auto& offset_name : offsets) {

		int value = std::stoul(reader.Get("Offsets", offset_name, "0x0"), nullptr, 16);
		
		if (value == 0x0) {
			std::cout << "Konnte offset von " << offset_name << " nicht laden." << std::endl;
		}
		else {
			std::cout << offset_name << " = " << value << std::endl;
			Offsets.set(offset_name, value);
		}
	}

	system("cls");
	
	SetConsoleTitle(TEXT("private external Hack - warte auf csgo.exe"));
	std::cout << "Warte auf csgo.exe ..." << std::endl;

	MemoryManager* memory_manager = new MemoryManager();

	// Waiting for the process "csgo.exe"
	while (!memory_manager->fetch_process("csgo.exe"));
	LocalPlayer local_player(memory_manager);
	
	UpdateCLIDisplay();
	SetConsoleTitle(TEXT("private external Hack - injected"));
	
	//std::thread vision_thread = std::thread(VisionThread, memory_manager, local_player);
	//std::thread activity_thread = std::thread(ActivityThread, memory_manager, local_player);
	
	do {

		VisionThread(memory_manager, local_player);
		ActivityThread(memory_manager, local_player);

	} while (true);


	delete memory_manager;

	system("pause");
    return 0;
}

